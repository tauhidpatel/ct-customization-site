import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SsoService {
  // private ssoUrl = 'https://localhost:7049';

  // constructor(private http: HttpClient) {}

  // performSso(sgid: string): Observable<any> {
  //   const url = `${this.ssoUrl}/api/Orders/CheckSSO/${sgid}`;
  //   return this.http.get(url);
  // }

  // performSso(sgid: string): Observable<any> {
  //   const url = `${this.ssoUrl}/api/Orders/CheckSSO/${sgid}`;
  //   return this.http.get(url, { responseType: 'text' }).pipe(
  //     map(response => ({ message: response }))
  //   );
  // }

  private ssoUrl = 'https://localhost:7049/api/sso1';

  constructor(private http: HttpClient) { }

  // authenticate(ticket: string): Observable<string> {
  //   return this.http.get<string>(`http://localhost:54100/api/sso1?ticket=${ticket}`);
  // }

  navigateToSsoPage(): void {
    // window.location.href = 'https://uat.websso.saint-gobain.com/cas/login' +
    //   '?service=' + encodeURIComponent('http://localhost:54100/api/sso1');
      window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "http://localhost:54100/" +"api/sso1/" ;
  }

  handleSsoCallback(): Observable<string> {
    // Make a request to your backend to retrieve SGID
    return this.http.get<string>(this.ssoUrl);
  }

  storeSgidInSession(sgid: string): void {
    sessionStorage.setItem('sgid', sgid);
  }

}
