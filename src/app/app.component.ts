import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, NgZone } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Okta } from 'src/app/shared/okta/okta.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ct-customization-site';

  user: any;
  oktaSignIn: any;

  supportLanguages = ['en', 'fr', 'es'];

  constructor(
    private okta: Okta, 
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private zone: NgZone,
    private http: HttpClient,
    private translateService: TranslateService
  ) 
  {
    this.oktaSignIn = okta.getWidget();

    this.translateService.addLangs(this.supportLanguages);
    this.translateService.setDefaultLang('fr')
    // debugger;
  } 

  logout(): void {
    this.oktaSignIn = this.okta.getWidget();
    // this.oktaSignIn.authClient.signOut(() => {
    //   this.router.navigate(['/login']); 
      
    // });
    this.oktaSignIn.authClient.signOut({
      clearTokensBeforeRedirect: true,
      postLogoutRedirectUri: 'http://localhost:4200'
    });
    // console.log(this.oktaSignIn.authClient.tokenManager);
    // this.oktaSignIn.signOut(() => {
    //   this.user = undefined;
    //   console.log(this.oktaSignIn.authClient.tokenManager);
    //   debugger;
      // this.router.navigate(['/login']); 
    // });
  }

  // logout(): void {
  //   this.oktaSignIn.authClient.tokenManager.clear();
    
  //   this.oktaSignIn = this.okta.getWidget();
  //   this.oktaSignIn.authClient.signOut(() => {
  //     this.router.navigate(['/login']); 
      
  //   });
  //   this.oktaSignIn.authClient.signOut({
  //     clearTokensBeforeRedirect: true,
  //     postLogoutRedirectUri: 'http://localhost:4200/login'
  //   });

  //   Sign out using Okta
  //   this.oktaSignIn.authClient.signOut({
  //     clearTokensBeforeRedirect: true,
  //     postLogoutRedirectUri: 'http://localhost:4200'
  //   })

    
  // }

  // login.component.ts
  // logout(): void {
  //   this.oktaSignIn.signOut(async () => {
  //     try {
  //       this.user = undefined;
  //       sessionStorage.removeItem("userId");

  //       this.oktaSignIn.remove();

  //       await this.oktaSignIn.authClient.session.close();

  //     } catch (error) {
  //       console.error("Error during logout:", error);
  //     }
  //   });
  // }


}
