import { Injectable } from '@angular/core';
import OktaSignIn from '@okta/okta-signin-widget';

@Injectable({
  providedIn: 'root'
})

export class Okta {
  
    widget;
 
    constructor() {
        this.widget = new OktaSignIn({
            logo: '//logo.clearbit.com/certainteed.com', 
            baseUrl: 'https://dev-34408392.okta.com',
            clientId: '0oaavecfyuB7SI1no5d7',
            redirectUri: 'http://localhost:4200'
        });
    }

    getWidget() {
        return this.widget;
    }

    logout(): void {
        this.widget.authClient.signOut({
          postLogoutRedirectUri: 'http://localhost:4200',
        });
    
        sessionStorage.removeItem('SGID');
        sessionStorage.clear();
        localStorage.clear();
    }
}